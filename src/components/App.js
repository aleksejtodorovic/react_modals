import React from 'react';
import Modal from './Modal';

const App = () => {
    return (
        <h1>
            Testing modals :)
            <Modal />
        </h1>
    );
};

export default App;